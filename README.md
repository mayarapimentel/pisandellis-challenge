ARCHAEOPTERYX 1.2.0-beta
==

What is this?
--
This is a scaffolding for build HTML pages using Sass and Pug. It handles a few things for you.  
Just write your Sass, Pug and Javascript files. Let Gulp do the rest like:

 * Compile, concatenate and minify your Sass Files
 * Compile your Pug files fill the dependencies
 * Lint your JS an Sass files
 * Reload all changes on save

Notes
--
Please, check the config folder first. It's not mandatory, but if you want to change some parameters, you can feel free to do so:

* `gulp.config.json`: Change the name of folders and files as you wish. Be sure to reflect the changes in files and folders that are already in this package.
* `sass.config.json`: add the folder's names as you wish. *Base, Layout, Modules* are based on SMACSS structure.  
 * Change `path` to reflect your folder structure;  
 * `starttag` and `endtag` are used to wrap the import files from the respective folder.  
 * `dependencies` is an array used to chain the compiling tasks.
* `pug.config.json` have some variables to use within pug files. Feel free to add or remove.

Get Ready
--
First run  
`npm install`

Then run  
`gulp setup`
This will create the inital folders for you.  
If you want to use it again later, just use `gulp start`.

CHANGELOG
==
[1.2.2-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.2.2-beta)
--
 * Remove Runsequence prior to gulp-sequence
 * Fix lang attributte in pug header

[1.2.1-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.2.1-beta)
--
 * Sorting Javascript injection

[1.2.0-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.2.0-beta)
--
 * Move out `config` folder from src

[1.1.3-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.1.3-beta)
--
 * Use config files in `JSON` format

[1.1.2-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.1.2-beta)
--
 * Fix Plumber error event not reloading browser after fixing error.
 * Delete .bowerrc file

[1.1.1-beta](https://gitlab.com/pisandelli/archaeopteryx/tags/v1.1.1-beta)
--
 * Update Packages
 * Clean Gulp tasks
 * Drop Bower

1.1.0-beta
--
 * Change Jade to Pug
 * Other minnor changes

1.0-beta
--
 * First Flight of a (new) Bird(?)

License
==
[GPL-v3](https://www.gnu.org/licenses/gpl-3.0.html)
